data "template_file" "docker_config_script" {
  template = "${file("config.json")}"
  vars = {
    registry_username           = "${var.registry_username}"
    registry_password           = "${var.registry_password}"
    registry_server             = "${var.registry_server}"
    registry_email              = "${var.registry_email}"
    auth                      = base64encode("${var.registry_username}:${var.registry_password}")
  }
}

resource "kubernetes_secret" "registry_secret" {
  metadata {
    name = "docker-cfg"
  }

  data = {
    ".dockerconfigjson" = "${data.template_file.docker_config_script.rendered}"
  }

  type = "kubernetes.io/dockerconfigjson"
}
