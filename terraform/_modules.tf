

module "cert-manager" {
  source  = "terraform-iaac/cert-manager/kubernetes"
  version = "2.4.2"
  # insert the 2 required variables here
  cluster_issuer_email                   = "${var.admin_email}"
  cluster_issuer_name                    = "cluster-issuer"
  cluster_issuer_private_key_secret_name = "cert-manager-private-key"
  depends_on = [
    helm_release.nginx_ingress_chart
  ]
}

