terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.13.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.3.0"
    }
  }
}


# Configure the DigitalOcean Provider
provider "digitalocean" {
  token = "${var.digitalocean_token}"
}
provider "kubernetes" {
  config_path   = "${local_file.kubernetes_config.filename}"
}
provider "kubectl" {
  # Same config as in kubernetes provider
  config_path   = "${local_file.kubernetes_config.filename}"
}
provider "helm" {
  kubernetes {
    config_path   = "${local_file.kubernetes_config.filename}"
    # Same config as in kubernetes provider
  }
}