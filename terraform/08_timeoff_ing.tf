resource "kubernetes_ingress_v1" "timeoff_ing" {
  wait_for_load_balancer = true
  metadata {
    name = "timeoff-ing"
    annotations = {
      "kubernetes.io/ingress.class" = "nginx"
      "cert-manager.io/cluster-issuer" = "cluster-issuer"
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }
  spec {
    rule {
      http {
        path {
          path = "/"
          path_type = "Prefix"
          backend {
            service {
              name = kubernetes_service.timeoff_svc.metadata.0.name
              port {
                number = 80
              }
            } 
          }
        }
      }
      host = "${var.timeoff_url}"
    }
    tls {
      hosts = [ "${var.timeoff_url}" ]
      secret_name = "timeoff-demo-tls-secret"
    }
  }
}