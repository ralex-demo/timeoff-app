resource "digitalocean_kubernetes_cluster" "timeoff" {
  name   = "timeoff"
  region = "nyc1"
  # Grab the latest version slug from `doctl kubernetes options versions`
  version = "1.24.4-do.0"

  node_pool {
    name       = "worker-pool"
    size       = "s-2vcpu-4gb"
    node_count = 1
  }
}

resource "kubernetes_secret" "digitalocean_token" {
  metadata {
    name = "do-access-token"
  }
  data = {
    "access-token" = base64encode("${var.digitalocean_token}")
  }
  
}