resource "kubernetes_deployment" "timeoff_deploy" {
  metadata {
    name = "timeoff-deploy"
    labels = {
      app = "timeoff-app"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = "timeoff-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "timeoff-app"
        }
      }

      spec {
        container {
          image = "${var.docker_image}"
          name  = "timeoff"
          port {
            container_port = 3000
          }
        }
        image_pull_secrets {
          name = "docker-cfg"
        }
      }
      
    }
  }
  depends_on = [
    kubernetes_secret.registry_secret
  ]
}