resource "kubernetes_service" "timeoff_svc" {
  metadata {
    name = "timeoff-svc"
    annotations = {
      "kubernetes.digitalocean.com/load-balancer-id" = digitalocean_loadbalancer.do_lb.id
    }
  }
  spec {
    type = "LoadBalancer"
    selector = {
      app = "${kubernetes_deployment.timeoff_deploy.metadata.0.labels.app}"
    }
    port {
      port        = 80
      target_port = 3000
    }
  }
}

