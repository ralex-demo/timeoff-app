data "kubernetes_service" "nginx_controller_svc" {
  metadata {
    name = "ingress-nginx-controller"
    namespace = "ingress-nginx"
  }
  depends_on = [ helm_release.nginx_ingress_chart ]
}


resource "digitalocean_record" "subdomain" {
  # domain = "digitalocean_domain.timeoff_domain.id"
  domain = "ralex.dev"
  name   = "${var.subdomain}"
  type   = "A"
  value  =  data.kubernetes_service.nginx_controller_svc.status.0.load_balancer.0.ingress.0.ip
  # value  = 
}